function checked() {
  var userName = document.getElementById("text").value;
  var userPhone = document.getElementById("phone").value;
  var checkBoxCheck = document.getElementsByName("checkers");
  var otherOne = document.getElementById("dropdown").value;
  var addInfo = document.getElementById("textarea").value;
  
  var checkValid = false;
  
  for(var i =0; i<checkBoxCheck.length; i++){
    if(checkBoxCheck[i].checked){
      checkValid = true;
      break;
    }
  }
  
  if(!checkValid){
    alert("Please check a checkbox")
  }
  
  if (userName == "" || userPhone == "") {
    alert("Please fill in: name, phone number, days of week.");
    if (otherOne == "Other") {
      if (addInfo == "") {
        alert("Please fill in your additional information.");
      }
    }
    return false;
  } else {
    return true;
  }
}